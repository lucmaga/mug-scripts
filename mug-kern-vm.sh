#!/bin/bash

set -eu

#if [[ "${PWD##*/}" != "linux" ]]; then
#	echo "ERROR: This script should be executed in a linux directory"
#	exit 1
#fi

readonly DIST="sid"
readonly VM_DIR="${HOME}/vms"
readonly IMG="${VM_DIR}/deb-${DIST}-vm.qcow2"
readonly MNT="${VM_DIR}/deb-${DIST}-vm-mount.dir"
readonly SHARE="./vm-share.dir"
readonly MODULES_SHARE_DIR="./tmp/lib/modules"
readonly KERNEL="arch/x86_64/boot/bzImage"

# PACKAGES TO INCLUDE IN THE IMG
# Base configuration
PKGS=openssh-server,xauth,xwayland
# Dev tools
PKGS=${PKGS},git,vim
# v4l-utils build dependencies
PKGS=${PKGS},dh-autoreconf,autotools-dev,doxygen,gettext,graphviz,libasound2-dev,libtool,libjpeg-dev,qtbase5-dev,libudev-dev,libx11-dev,pkg-config,udev,qt5-default

function vm_mount {
	if [[ ! -d "${MNT}" ]]; then
		echo "Creating folder ${MNT} to mount the image when required"
		mkdir "${MNT}"
	fi
	guestmount -a "${IMG}" -i "${MNT}"
	echo "${IMG} mounted at ${MNT}"
}

function vm_umount {
	guestunmount "${MNT}"
	echo "${IMG} umounted from ${MNT}"
}

function create_img {
	if [[ -f "${IMG}" ]]; then
		echo "${IMG} already exists, nothing to do"
		exit 1
	fi

	sudo vmdebootstrap --verbose --image="${IMG}" --size=5g --distribution="${DIST}" --grub --enable-dhcp --package="${PKGS}" --owner="${USER}"
}

function config_img {
	vm_mount

	# Add host folder to mount automatically
	if [[ ! -d "${SHARE}" ]]; then
		echo "Creating folder ${SHARE} to share files with the guest"
		mkdir "${SHARE}"
		echo "This is a shared folder between the host and guest" > "${SHARE}/README"
	fi
	echo "host-code /root/host 9p trans=virtio 0 0" >> "${MNT}/etc/fstab"

	# Add ssh key
	if [[ ! -f ~/.ssh/kern-vm-key ]]; then
		ssh-keygen -t rsa -N "" -f ~/.ssh/kern-vm-key -C root
	fi
	mkdir "${MNT}/root/.ssh"
	cat ~/.ssh/kern-vm-key.pub >> "${MNT}/root/.ssh/authorized_keys"

	# Enable X forward
	touch "${MNT}/root/.Xauthority"

	vm_umount
}

function vm_launch_native {
	# Launch VM with the kernel it is already installed
	kvm -hda "${IMG}" \
		-fsdev local,id=fs1,path="${MODULES_SHARE_DIR}",security_model=none \
		-device virtio-9p-pci,fsdev=fs1,mount_tag=host-code \
		-net nic -net user,hostfwd=tcp::5555-:22 \
		-m 2g
}

function vm_launch {
	# Launch VM with custom kernel
	kvm -hda "${IMG}" \
		-fsdev local,id=fs1,path="${MODULES_SHARE_DIR}",security_model=none \
		-device virtio-9p-pci,fsdev=fs1,mount_tag=host-code \
		-s \
		-smp 1 \
		-nographic \
		-kernel "${KERNEL}" \
		-append "root=/dev/sda1 console=ttyS0" \
		-net nic -net user,hostfwd=tcp::5555-:22 \
		-m 1g
}

case "${1-}" in
	mount)
		vm_mount
		;;
	umount)
		vm_umount
		;;
	install)
		vm_mount
		make modules_install install INSTALL_MOD_PATH="${MNT}" INSTALL_PATH="${MNT}/boot"
		vm_umount
		;;
	modules_install)
		vm_mount
		INSTALL_MOD_PATH="${MNT}" make modules_install
		vm_umount
		;;
	launch)
		vm_launch
		;;
	launch-native)
		vm_launch_native
		;;
	create-img)
		create_img
		config_img
		;;
	*)
		echo "Usage: $0 {mount|umount|install|launch|launch-native|create-img}"
		echo "Requirements: libguestfs-tools kvm vmdebootstrap"
		exit 1
esac
